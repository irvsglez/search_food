class Divisible
  def initialize(number)
    @number = number.to_i
    @met_rules = false
    @word = ''
  end

  DIVISIBLES = [3, 5, 7].freeze

  def check_argvs
    raise 'Argument missing!' unless ARGV.count == 1
  end

  def print
    DIVISIBLES.each { |divisible| check_divisible(divisible) }

    p @word unless @word.empty?
    p @number unless @met_rules
  end

  private

  def divisible_by?(number, number1)
    number % number1 == 0
  end

  def print_word(num)
    case num
    when 3
      'my'
    when 5
      'theresa'
    when 7
      'clothes'
    else
      num
    end
  end

  def check_divisible(divisible)
    if divisible_by?(@number, divisible)
      @word = "#{@word}#{print_word(divisible)}"
      @met_rules = true
    end
  end
end

printer = Divisible.new(ARGV[0])
printer.check_argvs
printer.print

# --- my_repos/Pruebas » ruby divisible.rb 3
# "my"
# --- my_repos/Pruebas » ruby divisible.rb 5
# "theresa"
# --- my_repos/Pruebas » ruby divisible.rb 7
# "clothes"
# --- my_repos/Pruebas » ruby divisible.rb 21
# "myclothes"
# --- my_repos/Pruebas » ruby divisible.rb 15
# "mytheresa"
# --- my_repos/Pruebas » ruby divisible.rb 13
# 13
# --- my_repos/Pruebas » ruby divisible.rb 2345
# "theresaclothes"
# --- my_repos/Pruebas » ruby divisible.rb 11                                                                                                                                                                     1 ↵
# 11
