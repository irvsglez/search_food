#!/usr/bin/env ruby

# This version only looks for a specific recipe (hardcoded).
require 'json'

def print_welcome
  puts ''.center(60, '=')
  puts " Looking for Cookie Monster Cupcake recipe ".center(60, '=')
  puts ''.center(60, '=')
end

def get_url
  `curl -X GET 'https://www.food2fork.com/api/search?key=e676ea4152b2077f7e7bef634e232fff&q=cookie%20monster%20cupcake'`
end

def count_right?(count)
  count == 1
end

def recipe_title_right?(recipe_title)
  recipe_title['title'].downcase == 'cookie monster cupcakes'
end

def print(response)
  puts ''.center(60, '=')
  puts 'Response includes the recipe we want:'
  puts JSON.pretty_generate(response)
end

def check_recipe(response)
  count = response['count']
  recipe = response['recipes'][0]

  print(response) if count_right?(count) && recipe_title_right?(recipe)
end

begin
  print_welcome
  check_recipe(JSON.parse get_url)
rescue => e
  puts "ERROR: #{e}"
end
