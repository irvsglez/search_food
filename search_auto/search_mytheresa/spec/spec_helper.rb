require 'capybara/rspec'
require 'capybara-screenshot'
require 'capybara/poltergeist'
require 'rspec/retry'
require 'byebug'

# Helpers requirements
require 'helpers/acceptance_helpers/base'

Capybara.javascript_driver = :poltergeist
Capybara.register_driver :poltergeist do |app|
  # When false, Javascript errors do not get re-raised in Ruby.
  Capybara::Poltergeist::Driver.new(app,
    js_errors: false,
    timeout: 10000,
    phantomjs_options:
      ['--load-images=no', '--ignore-ssl-errors=yes', '--ssl-protocol=any'])
end

Capybara.save_and_open_page_path = './tmp/screenshots/'
Capybara.run_server = false
Capybara.default_wait_time = 20

# Set the app_host for Capybara

puts "Testing Food2fork ... BEHOLD!"
Capybara.app_host = 'https://www.food2fork.com'

RSpec.configure do |config|
  config.include(AcceptanceHelpers::Base, type: :feature)
  config.verbose_retry = true

  config.after(:each) do |example|
    if example.exception && example.metadata[:js]
      path = "#{Dir.pwd}/tmp/capybara_screenshots/#{Time.now.strftime '%Y%m%d-%H%M%S'}.png"
      puts "\n"
      puts "Failed spec: #{example.full_description}"
      puts " -> Exception: #{example.exception}"
      page.save_screenshot(path)
      puts " -> Saved screenshot in #{path}"
    end
  end
end

class << self
  def feature_with_retry(*args, &block)
    options = args.last.is_a?(Hash) ? args.pop : {}
    options[:retry] = 5 if options[:js] && !ENV['RSPEC_NO_RETRY']
    args.push(options)

    feature_without_retry(*args, &block)
  end
end
