# encoding: utf-8
feature 'SearchFood', js: true do
  let(:home_page) { '/' }
  let(:food) { 'Cookie Monster cupcakes' }

  context 'Searching for a receipt' do
    it 'finds the receipt' do
      visit home_page
      search_content(food)
      wait_for_element('.recipe-name', 10)

      # if we know in advanced the amount of recipes that we are going to get
      # we can also check it as shown in the next line:
      count = page.evaluate_script("$('.recipe-name').length;")
      expect(count).to eq(1)

      recipe = page.evaluate_script("$('.recipe-name')[0].innerHTML;")
      expect(recipe).to have_content(food)

      # We can also check that we are getting only the recipe we want checking
      # the name of other one. But it would be better to check only the amount
      # of results as shown before.
      expect(page).to_not have_content('Jalapeno Popper Grilled Cheese Sandwich')
    end
  end
end
