# -*- coding: utf-8 -*-
module AcceptanceHelpers
end

module AcceptanceHelpers::Base

  # Helper build to manage the base stuff to be performed in
  # the tests. Other helpers for more specific features would be
  # placed in other modules.

  def wait_for_element(consequence, time)
    previous_time = Capybara.default_wait_time
    Capybara.default_wait_time = time
    find(:css, consequence)
    Capybara.default_wait_time = previous_time
  end

  def search_content(content)
    find('#typeahead').native.send_keys("#{content}\n")
  end
end
